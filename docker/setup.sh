#!/bin/bash

echo "SETUP"
cd app

composer install
./artisan migrate:install
./artisan migrate:status
./artisan migrate

cd resources/frontend/angular
npm install