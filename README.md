# COCO Blog AI

## Installation

Clone this repository

CD into the project directory and run
```
docker exec -i coco-app bash < docker/setup.sh
```

Set the `OPENAI_API_KEY` in `.env`

## Running dev mode

You can start the dev environment with:
```
docker compose up -d
```

The app should be up and running under: http://localhost:8000

## Angular local dev

Run:

```
docker exec -i coco-app bash < docker/watch.sh
```
