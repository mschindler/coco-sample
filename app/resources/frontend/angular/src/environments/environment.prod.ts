export const environment = {
  production: true,
  // change as needed
  appUrl: 'localhost:8000',
  apiHost: 'http://localhost:8000/api',
};
