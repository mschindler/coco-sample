export const environment = {
  production: false,
  appUrl: 'localhost:8000',
  apiHost: 'http://localhost:8000/api',
};
