import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RssSelectorComponent } from './rss-selector.component';

describe('RssSelectorComponent', () => {
  let component: RssSelectorComponent;
  let fixture: ComponentFixture<RssSelectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RssSelectorComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RssSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
