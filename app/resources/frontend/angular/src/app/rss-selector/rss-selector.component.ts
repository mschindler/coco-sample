import { Component } from '@angular/core';
import {NgForOf, NgIf} from "@angular/common";
import {RssService} from "../_service/rss.service";
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {MatFormField, MatOption, MatSelect, MatLabel} from '@angular/material/select'
import {FeedSource} from "../_interface/RssFeedSourceEntry";
import {MatButtonModule} from "@angular/material/button";
import {FeedListItem} from "../_interface/FeedListItem";
import {FeedListItemComponent} from "../feed-list-item/feed-list-item.component";
import {MatProgressBar} from "@angular/material/progress-bar";
import {BlogSettingsPanelComponent} from "../blog-settings-panel/blog-settings-panel.component";
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-rss-selector',
  standalone: true,
  imports: [
    NgForOf,
    MatSlideToggleModule,
    MatSelect,
    MatFormField,
    MatOption,
    MatLabel,
    MatButtonModule,
    NgIf,
    FeedListItemComponent,
    MatProgressBar,
    BlogSettingsPanelComponent,
    FormsModule
  ],
  templateUrl: './rss-selector.component.html',
  styleUrl: './rss-selector.component.less'
})
export class RssSelectorComponent {
    title: string = "Bitte wähle deinen RSS-Feed";
    selectedRssSourceId?: number;
    rssFeedSources: FeedSource[] = [];
    feedListItems?: FeedListItem[] = [];
    feedIsLoading: boolean = false;
    selectedItem?: FeedListItem

    constructor(private service: RssService) {
      service.getRssFeedSources().subscribe(data => {
            this.rssFeedSources = data;
        });
    }

    onChangeValue(event?: any): void {
        this.selectedRssSourceId = event.value;
    }

    loadFeed(feedId?: number): void {
        this.feedIsLoading = true;
        this.service.getRssFeedById(feedId).subscribe(data => {
            this.feedIsLoading = false;
            this.feedListItems = data;
        })
    }

  listItemClicked(event?: any): void {
      this.feedListItems?.forEach((item) => item.active = (item == event));
      this.selectedItem = event ? event : undefined;
  }
}
