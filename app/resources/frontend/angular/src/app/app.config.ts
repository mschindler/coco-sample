import { ApplicationConfig, provideZoneChangeDetection } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import {RssService} from "./_service/rss.service";
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import {provideHttpClient, withInterceptorsFromDi} from "@angular/common/http";
import {BlogArticleService} from "./_service/blogArticle.service";
import {StateService} from "./_service/state.service";
import {BackendApiService} from "./_service/backend-api.service";

export const appConfig: ApplicationConfig = {
  providers: [
    provideZoneChangeDetection({ eventCoalescing: true }), provideRouter(routes),
    BackendApiService, RssService, BlogArticleService, StateService, provideAnimationsAsync(), provideHttpClient(withInterceptorsFromDi())
  ],
};
