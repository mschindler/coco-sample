import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {RssSelectorComponent} from "./rss-selector/rss-selector.component";
import {StateService} from "./_service/state.service";
import {NgIf} from "@angular/common";
import {BlogArticleComponent} from "./blog-article/blog-article.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RssSelectorComponent, NgIf, BlogArticleComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.less'
})
export class AppComponent {
    constructor(public stateService: StateService) {}
}
