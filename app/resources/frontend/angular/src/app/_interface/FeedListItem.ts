export interface FeedListItem {
  title: string,
  link: string,
  image: string,
  active: boolean
}
