export interface BlogArticleResponse {
  title: string,
  introduction: string,
  body: string,
  metaTitle: string,
  metaDescription: string,
  metaKeywords: string[],
}
