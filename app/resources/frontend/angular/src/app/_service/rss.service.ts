import {Injectable} from "@angular/core";
import {FeedSource} from "../_interface/RssFeedSourceEntry";
import {Observable} from "rxjs";
import {BackendApiService} from "./backend-api.service";

@Injectable()
export class RssService {

    constructor(private backendApiService: BackendApiService) { }

    getRssFeedSources(): Observable<FeedSource[]> {
        return this.backendApiService.getRssFeedSources();
    }

    getRssFeedById(feedId?: number) {
        return this.backendApiService.getRssFeedById(feedId);
    }
}
