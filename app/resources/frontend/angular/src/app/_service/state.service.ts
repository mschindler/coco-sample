import {Injectable} from "@angular/core";
import {BlogArticleResponse} from "../_interface/BlogArticleResponse";

@Injectable()
export class StateService {
  blogArticleLoaded: boolean = false;
  blogArticle?: BlogArticleResponse;
  articleIsLoading: boolean = false;

  resetBlogArticleState(): void
  {
      this.blogArticleLoaded = false;
      this.blogArticle = undefined;
  }
}
