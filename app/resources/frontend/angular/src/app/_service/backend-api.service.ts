import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {catchError, Observable, tap, throwError} from "rxjs";
import {FeedSource} from "../_interface/RssFeedSourceEntry";
import {FeedListItem} from "../_interface/FeedListItem";
import {BlogArticleResponse} from "../_interface/BlogArticleResponse";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root'
})
export class BackendApiService {
  private baseUrl: string = environment.apiHost;

  constructor(
    private httpClient: HttpClient,
    private snackBar: MatSnackBar
  ) { }

  getRssFeedSources(): Observable<FeedSource[]> {
    const url: string = this.baseUrl + '/feed-sources'
    return this.get<FeedSource[]>(url);
  }

  getRssFeedById(feedId?: number) {
    const url: string = this.baseUrl + '/feed-sources/' + feedId;
    return this.get<FeedListItem[]>(url);
  }

  generateBlogArticle(feedItem: FeedListItem, pronoun: string, style: string): Observable<BlogArticleResponse> {
    const url: string = this.baseUrl + '/blog-article';
    return this.post<BlogArticleResponse>(url, {
      topic: feedItem.title,
      pronoun: pronoun,
      style: style
    });
  }

  private post<T>(url: string, body: any): Observable<T> {
    return this.httpClient.post<T>(url, body).pipe(
      catchError(this.handleError)
    );
  }

  private get<T>(url: string): Observable<T> {
    return this.httpClient.get<T>(url).pipe(
      catchError(this.handleError)
    );
  }

  private handleError = (err: HttpErrorResponse) => {
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      errorMessage = `An error occured: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.log(errorMessage);
    this.snackBar.open(errorMessage, 'dismiss', {duration: 5000})

    return throwError(() => errorMessage);
  }
}
