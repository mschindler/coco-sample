import {Injectable} from "@angular/core";
import {FeedListItem} from "../_interface/FeedListItem";
import {Observable} from "rxjs";
import {BlogArticleResponse} from "../_interface/BlogArticleResponse";
import {BackendApiService} from "./backend-api.service";

@Injectable()
export class BlogArticleService {

    constructor(private backendApiService: BackendApiService) { }

    generateBlogArticle(feedItem: FeedListItem, pronoun: string, style: string): Observable<BlogArticleResponse> {
        return this.backendApiService.generateBlogArticle(feedItem, pronoun, style);
    }
}
