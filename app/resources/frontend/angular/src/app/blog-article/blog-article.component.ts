import { Component } from '@angular/core';
import {StateService} from "../_service/state.service";
import {BlogArticleResponse} from "../_interface/BlogArticleResponse";
import {MatDivider} from "@angular/material/divider";
import {MatIcon} from "@angular/material/icon";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Clipboard} from '@angular/cdk/clipboard';

@Component({
  selector: 'app-blog-article',
  standalone: true,
  imports: [
    MatDivider,
    MatIcon
  ],
  templateUrl: './blog-article.component.html',
  styleUrl: './blog-article.component.less'
})
export class BlogArticleComponent {
    blogArticle?: BlogArticleResponse
    constructor(private stateService: StateService, private clipboard: Clipboard, private snackBar: MatSnackBar) {
        this.blogArticle = stateService.blogArticle;
    }

    copyBlogArticleToClipboard(): void {
      this.copyTextToClipboard(this.blogArticle?.title + "\n" + this.blogArticle?.body)
      this.notify('Artikel');
    }

    copyMetaDescriptionToClipboard(): void {
      this.copyTextToClipboard(this.blogArticle?.metaDescription + '')
      this.notify('Meta-Description');
    }

    copyMetaTitleToClipboard(): void {
      this.copyTextToClipboard(this.blogArticle?.metaTitle + '')
      this.notify('Meta-Titel');
    }

    copyMetaKeywordsToClipboard(): void {
      this.copyTextToClipboard(this.blogArticle ? this.blogArticle.metaKeywords.join(', ') : '');
      this.notify('Meta-Keywords');
    }

    private notify(typeOfCopy: string): void {
      this.snackBar.open(`${typeOfCopy} wurde in die Zwischenablage kopiert`, undefined, {duration: 2000});
    }

    private copyTextToClipboard(text: string): void {
      const pending = this.clipboard.beginCopy(text);
      let remainingAttempts = 3;
      const attempt = () => {
        const result = pending.copy();
        if (!result && --remainingAttempts) {
          setTimeout(attempt);
        } else {
          // Remember to destroy when you're done!
          pending.destroy();
        }
      };
      attempt();
    }
}
