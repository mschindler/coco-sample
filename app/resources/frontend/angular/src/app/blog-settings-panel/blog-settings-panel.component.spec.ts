import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogSettingsPanelComponent } from './blog-settings-panel.component';

describe('BlogSettingsPanelComponent', () => {
  let component: BlogSettingsPanelComponent;
  let fixture: ComponentFixture<BlogSettingsPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BlogSettingsPanelComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BlogSettingsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
