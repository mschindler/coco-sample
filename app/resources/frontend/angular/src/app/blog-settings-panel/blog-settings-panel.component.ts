import {Component, Input} from '@angular/core';
import {MatButtonToggle, MatButtonToggleGroup} from "@angular/material/button-toggle";
import {MatDivider} from "@angular/material/divider";
import {MatButton} from "@angular/material/button";
import {FeedListItem} from "../_interface/FeedListItem";
import {NgIf} from "@angular/common";
import {BlogArticleService} from "../_service/blogArticle.service";
import {MatProgressBar} from "@angular/material/progress-bar";
import {StateService} from "../_service/state.service";

@Component({
  selector: 'app-blog-settings-panel',
  standalone: true,
  imports: [
    MatButtonToggleGroup,
    MatButtonToggle,
    MatDivider,
    MatButton,
    NgIf,
    MatProgressBar
  ],
  templateUrl: './blog-settings-panel.component.html',
  styleUrl: './blog-settings-panel.component.less'
})
export class BlogSettingsPanelComponent {
  @Input() selectedItem?: FeedListItem
  selectedStyle?: string;
  selectedPronoun?: string;

  constructor(
    private blogArticleService: BlogArticleService,
    public stateService: StateService
  ) {}

  onChangeStyle(event?: any): void {
    this.selectedStyle = event.value;
  }

  onChangePronoun(event?: any): void {
    this.selectedPronoun = event.value;
  }

  createBlogArticle() {
    if (this.selectedItem && this.selectedPronoun && this.selectedStyle) {
      this.stateService.resetBlogArticleState()
      this.stateService.articleIsLoading = true;
      this.blogArticleService.generateBlogArticle(this.selectedItem, this.selectedPronoun, this.selectedStyle).subscribe({
        next: (data) => {
          this.stateService.blogArticleLoaded = true;
          this.stateService.articleIsLoading = false;
          this.stateService.blogArticle = data;
        },
        error: (error) => {
          this.stateService.resetBlogArticleState();
          this.stateService.articleIsLoading = false;
        }
      });
    }
  }
}
