import {Component, Input} from '@angular/core';
import {FeedListItem} from "../_interface/FeedListItem";
import {MatDivider} from "@angular/material/divider";
import {NgClass} from "@angular/common";

@Component({
  selector: 'app-feed-list-item',
  standalone: true,
  imports: [
    MatDivider,
    NgClass
  ],
  templateUrl: './feed-list-item.component.html',
  styleUrl: './feed-list-item.component.less'
})
export class FeedListItemComponent {
    @Input() feedListItem?: FeedListItem

    isSelected?: boolean = this.feedListItem?.active;
}
