<?php

namespace App\Repositories;

use App\Contracts\Repositories\FeedSourceRepository;
use Illuminate\Support\Collection;

class InMemoryFeedSourceRepository implements FeedSourceRepository
{
    private array $data = [
        [
            'id' => 1,
            'name' => 'Bundesministerium für Wirtschaft und Klimaschutz',
            'url' => 'https://www.bmwk.de/SiteGlobals/BMWI/Functions/RSSFeed/DE/RSSFeed-Wirtschaft.xml'
        ],
        [
            'id' => 2,
            'name' => 'handwerk.com',
            'url' => 'https://www.handwerk.com/rss.xml'
        ],
        [
            'id' => 3,
            'name' => 'Golem - Wirtschaft',
            'url' => 'https://rss.golem.de/rss.php?ms=wirtschaft&feed=RSS2.0'
        ],
    ];


    public function fetchAllSources(): Collection
    {
        return Collection::make($this->data);
    }

    public function hasFeed(int $feedId): bool
    {
        return Collection::make($this->data)->where('id', $feedId)->isNotEmpty();
    }

    public function fetchFeed(int $feedId): array
    {
        return (array) Collection::make($this->data)->where('id', $feedId)->first();
    }
}
