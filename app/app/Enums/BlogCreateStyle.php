<?php

namespace App\Enums;

enum BlogCreateStyle : string
{
    case FRIENDLY = 'friendly';
    case PROFESSIONAL = 'professional';
    case CONVINCING = 'convincing';
    case CASUAL = 'casual';
}
