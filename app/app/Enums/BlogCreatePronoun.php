<?php

namespace App\Enums;

enum BlogCreatePronoun : string
{
    case PERSONAL = 'personal';
    case FORMAL = 'formal';
}
