<?php

namespace App\Contracts\Repositories;

use Illuminate\Support\Collection;

interface FeedSourceRepository
{
    public function fetchAllSources(): Collection;

    public function hasFeed(int $feedId): bool;

    public function fetchFeed(int $feedId);
}
