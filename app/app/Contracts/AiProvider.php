<?php

namespace App\Contracts;

use App\Enums\BlogCreatePronoun;
use App\Enums\BlogCreateStyle;

interface AiProvider
{
    public function request(string $topic, BlogCreatePronoun $pronoun, BlogCreateStyle $style): AiProviderResponse;
}
