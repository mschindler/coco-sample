<?php

namespace App\Contracts;

use Illuminate\Support\Collection;

interface RssFeedFetcher
{
    public function fetch(int $feedId): Collection;
}
