<?php

namespace App\Services\ChatGptBlogPostCreator;

use App\Contracts\AiProvider;
use App\Enums\BlogCreatePronoun;
use App\Enums\BlogCreateStyle;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;

readonly class ChatGptBlogPostCreatorService implements AiProvider
{
    public function __construct(
        private string $url,
        private string $apiKey,
        private string $model,
        private float  $temperature,
        private int    $maxTokens,
    ) {}

    /**
     * @throws ConnectionException
     * @throws RequestException
     */
    public function request(string $topic, BlogCreatePronoun $pronoun, BlogCreateStyle $style): ChatGptResponse
    {
        $userPrompt = \sprintf("Please create a blog post about following topic: \"%s\" but create a custom title", $topic);

        $messages = [
            [
                'role' => 'system',
                'content' => implode("\n", $this->assembleSystemPrompt($pronoun, $style))
            ],
            [
                'role' => 'user',
                'content' => $userPrompt,
            ]
        ];

        $response = Http::withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => \sprintf('Bearer %s', $this->apiKey),
        ])->post($this->url, [
            'model' => $this->model,
            'response_format' => ["type" => "json_object"],
            'messages' => $messages,
            'temperature' => $this->temperature,
            'max_tokens' => $this->maxTokens,
        ])->throwUnlessStatus(200)->body();


        $responseArray = \json_decode($response, true);

        return ChatGptResponse::fromArray($responseArray);
    }

    private function assembleSystemPrompt(BlogCreatePronoun $blogCreatePronoun, BlogCreateStyle $blogCreateStyle): array
    {
        $pronounTranslated = match($blogCreatePronoun) {
            BlogCreatePronoun::FORMAL => 'Sie',
            BlogCreatePronoun::PERSONAL => 'Du'
        };

        return [
            'You are a marketing specialist.',
            'You only respond with valid JSON',
            \sprintf('The user wants to create a blog post. The blog post has to be in a %s style', $blogCreateStyle->value),
            'The blog post has to be in german language',
            \sprintf('Please use the pronoun "%s" when creating the blog post', $pronounTranslated),
            'The blog post has to be around 1000 characters long.',
            'The structure of the blog post text has to be as follows: A catchy and descriptive title. Introduction text with around 160 characters which matches the title.',
            'Introductory paragraph, emphasizing relevance to the target group',
            'Do not use the users input as the title of the blog post!',
            'Pay attention to create paragraphs. The blog post has to be SEO optimized.',
            'Format the blog post in HTML',
            'Do not include the title in the body',
            'Additionally create following seo attributes, matching the created blog post: ',
            'meta title, meta description, and at least 5 meta keywords.',
            'Before sending the response, please self-assess your result in 3 iterations to improve the quality of the response.',
            'Finally, your response should only be a valid JSON with following keys:',
            'title, introduction, body, meta_title, meta_description, meta_keywords. meta_keywords has to be an array of keywords'
        ];
    }
}
