<?php
namespace App\Services\ChatGptBlogPostCreator;

use App\Contracts\AiProviderResponse;

class ChatGptResponse implements AiProviderResponse, \JsonSerializable
{

    public static function dummy(): self
    {
        return new self(
            '{}',
            'Bereit für die EM 2024: So kommen Sie in Fußballstimmung',
            'Der 14. Juni markiert den Start der EM 2024. Sind Sie schon in Fußballstimmung? Hier erfahren Sie, wie Sie sich optimal vorbereiten.',
            'Die Europameisterschaft 2024 steht vor der Tür und der 14. Juni ist der lang ersehnte Anpfiff. Für Fußballfans in ganz Europa ist dies ein besonderes Ereignis, das nicht nur die Herzen höher schlagen lässt, sondern auch Gemeinschaft und Begeisterung fördert. Für viele ist die EM eine Zeit, in der man sich mit Freunden und Familie trifft, um die Spiele gemeinsam zu verfolgen. Doch wie können Sie sicherstellen, dass Sie wirklich in EM-Stimmung sind? Hier sind einige Tipps, um die Vorfreude zu steigern 1. Dekorieren Sie Ihr Zuhause: Schmücken Sie Ihr Zuhause mit den Farben Ihres Lieblingsteams. Fahnen, Banner und andere Fanartikel können die richtige Atmosphäre schaffen. 2. Planen Sie Fußballabende: Organisieren Sie Fußballabende mit Freunden und Familie. Gemeinsam macht das Zuschauen noch mehr Spaß. 3. Informieren Sie sich: Lesen Sie sich in die Teams und Spieler ein. Je mehr Sie über die Teilnehmer wissen, desto spannender wird das Turnier. 4. Besuchen Sie Public Viewings: Viele Städte bieten Public Viewings an, bei denen Sie die Spiele auf großen Leinwänden verfolgen können. Die Stimmung dort ist oft unvergesslich. Indem Sie diese Tipps befolgen, können Sie sicherstellen, dass Sie bestens auf die EM 2024 vorbereitet sind und die Spiele in vollen Zügen genießen können. Lassen Sie sich von der Begeisterung anstecken und feiern Sie dieses Fußballfest mit anderen Fans!',
            'Bereit für die EM 2024: So kommen Sie in Fußballstimmung',
            'Der 14. Juni markiert den Start der EM 2024. Sind Sie schon in Fußballstimmung? Erfahren Sie hier, wie Sie sich optimal vorbereiten und die Spiele genießen.',
            [
                'EM 2024',
                'Fußballstimmung',
                'Europameisterschaft',
                'Fußballfans',
                'Public Viewing'
            ]
        );
    }

    public static function fromArray(array $gptResponseArray): self
    {
        // @todo: Do validation here

        $encodedResponseContent = json_decode($gptResponseArray['choices'][0]['message']['content'], true);

        return new self(
            \json_encode($gptResponseArray),
            $encodedResponseContent['title'],
            $encodedResponseContent['introduction'],
            $encodedResponseContent['body'],
            $encodedResponseContent['meta_title'],
            $encodedResponseContent['meta_description'],
            $encodedResponseContent['meta_keywords']
        );
    }

    private function __construct(
        public string $rawJsonResponse,
        public string $title,
        public string $introduction,
        public string $body,
        public string $metaTitle,
        public string $metaDescription,
        public array $metaKeywords
    ) {}

    public function jsonSerialize(): array
    {
        return [
            'title' => $this->title,
            'introduction' => $this->introduction,
            'body' => $this->body,
            'metaTitle' => $this->metaTitle,
            'metaDescription' => $this->metaDescription,
            'metaKeywords' => $this->metaKeywords,
        ];
    }
}
