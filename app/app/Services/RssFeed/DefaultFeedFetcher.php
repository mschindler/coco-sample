<?php

namespace App\Services\RssFeed;

use App\Contracts\Repositories\FeedSourceRepository;
use App\Contracts\RssFeedFetcher;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class DefaultFeedFetcher implements RssFeedFetcher
{
    public function __construct(private readonly FeedSourceRepository $feedSourceRepository) {}

    public function fetch(int $feedId): Collection
    {
        if (!$this->feedSourceRepository->hasFeed($feedId)) {
            throw new \Exception("Feed not found");
        }

        $feedSource = $this->feedSourceRepository->fetchFeed($feedId);
        $content = Http::get($feedSource['url'])->body();

        $xml = simplexml_load_string($content);

        $items = Collection::make();
        foreach ($xml->channel->item as $item) {
            $items->add(
                FeedItem::create(
                    $item->title,
                    $item->link,
                    (string) $xml->channel->item[0]?->enclosure?->attributes()?->url
                )
            );
        }

        return $items;
    }
}
