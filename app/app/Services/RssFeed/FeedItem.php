<?php

namespace App\Services\RssFeed;

readonly class FeedItem implements \JsonSerializable
{
    public static function create(string $title, string $link, string $image): self
    {
        return new self($title, $link, $image);
    }

    private function __construct(
        private string $title,
        private string $link,
        private string $image
    ) {}

    public function jsonSerialize(): array
    {
        return [
            'title' => $this->title,
            'link' => $this->link,
            'image' => $this->image,
        ];
    }
}
