<?php

namespace App\Http\Controllers\Api;

use App\Contracts\AiProvider;
use App\Enums\BlogCreatePronoun;
use App\Enums\BlogCreateStyle;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateBlogArticleRequest;
use Illuminate\Http\JsonResponse;

class CreateBlogArticleController extends Controller
{
    public function __invoke(CreateBlogArticleRequest $request, AiProvider $aiProviderService): JsonResponse
    {
//        return response()->json(
//            ChatGptResponse::dummy()
//        );

        return response()->json(
            $aiProviderService->request(
                $request->get('topic', ''),
                BlogCreatePronoun::from($request->get('pronoun', '')),
                BlogCreateStyle::from($request->get('style', ''))
            )
        );
    }
}
