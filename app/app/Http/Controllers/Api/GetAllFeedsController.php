<?php

namespace App\Http\Controllers\Api;

use App\Contracts\RssFeedFetcher;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class GetAllFeedsController extends Controller
{
    public function __invoke(int $feedId, RssFeedFetcher $feedFetcher): JsonResponse
    {
        return response()->json($feedFetcher->fetch($feedId));
    }
}
