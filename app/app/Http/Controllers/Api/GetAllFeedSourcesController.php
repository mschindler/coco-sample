<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\FeedSourceRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class GetAllFeedSourcesController extends Controller
{
    public function __invoke(FeedSourceRepository $feedSourceRepository): JsonResponse
    {
        return response()->json($feedSourceRepository->fetchAllSources());
    }
}
