<?php

namespace App\Http\Requests;

use App\Enums\BlogCreatePronoun;
use App\Enums\BlogCreateStyle;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class CreateBlogArticleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'topic' => 'required|max:255',
            'pronoun' => ['required', Rule::enum(BlogCreatePronoun::class)],
            'style' => ['required', Rule::enum(BlogCreateStyle::class)],
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            response()->json(
                [
                    'success'   => false,
                    'message'   => 'Validation errors',
                    'data'      => $validator->errors()
                ],
                400
            )
        );
    }
}
