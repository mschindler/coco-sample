<?php

namespace App\Providers;

use App\Contracts\AiProvider;
use App\Contracts\Repositories\FeedSourceRepository;
use App\Contracts\RssFeedFetcher;
use App\Repositories\InMemoryFeedSourceRepository;
use App\Services\ChatGptBlogPostCreator\ChatGptBlogPostCreatorService;
use App\Services\RssFeed\DefaultFeedFetcher;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public array $bindings = [
        AiProvider::class => ChatGptBlogPostCreatorService::class,
        RssFeedFetcher::class => DefaultFeedFetcher::class,
    ];

    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(FeedSourceRepository::class, InMemoryFeedSourceRepository::class);

        $this->app->singleton(ChatGptBlogPostCreatorService::class, function() {
            return new ChatGptBlogPostCreatorService(
                url: Config::get('chatgpt.url'),
                apiKey: Config::get('chatgpt.key'),
                model: Config::get('chatgpt.model'),
                temperature: Config::get('chatgpt.temperature'),
                maxTokens: Config::get('chatgpt.max_tokens'),
            );
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->configureRatelimiting();
    }

    protected function configureRateLimiting(): void
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(10)->by($request->ip());
        });
    }
}
