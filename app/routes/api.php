<?php

use App\Http\Controllers\Api\CreateBlogArticleController;
use App\Http\Controllers\Api\GetAllFeedSourcesController;
use App\Http\Controllers\Api\GetAllFeedsController;
use Illuminate\Support\Facades\Route;

Route::get('/feed-sources', [GetAllFeedSourcesController::class, '__invoke']);
Route::get('/feed-sources/{feedId}', [GetAllFeedsController::class, '__invoke']);
Route::post('/blog-article', [CreateBlogArticleController::class, '__invoke']);
