<?php

return [
    'url' => 'https://api.openai.com/v1/chat/completions',
    'key' => env('OPENAI_API_KEY'),
    'model' => 'gpt-4o',
    'temperature' => 0.3,
    'max_tokens' => 4096
];
